package com.android.systemui.statusbar.phone;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.mods.grx.GrxModsSharedMethods;
import com.mods.grx.GrxSettings;

/**
 * Created by Grouxho 04/02/2018.
 */

public class KeyguardStatusBarView extends RelativeLayout {

    ImageView mTwoPhoneModeIcon; // do not add it already exists in smali, i need it for convenience.



    //add these 2 vars to smali
    LinearLayout grxLeftContainerView, grxCenterContainerView, grxRightContainerView;
    private String grxOldKgSbOrder;




    public KeyguardStatusBarView(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    public KeyguardStatusBarView(Context context){
        super(context);

    }


    @Override
    protected void onFinishInflate(){
        super.onFinishInflate();

        grxSetUPInitialPositions(); //add this call at the end of the Onfinishinflate


    }

    // add these 2 methods to smalies


    public void grxSetUPInitialPositions(){
        if(mTwoPhoneModeIcon!=null) mTwoPhoneModeIcon.setTag("twophones"); // in s7e  the view is added in onfinishinflated
        int aux = getContext().getResources().getIdentifier("keyguard_notification_icon_area_inner","id",getContext().getPackageName());
        grxLeftContainerView = (LinearLayout) findViewById(aux);
        aux = getContext().getResources().getIdentifier("center_container","id",getContext().getPackageName());
        grxCenterContainerView = (LinearLayout) findViewById(aux);
        aux = getContext().getResources().getIdentifier("system_icons","id",getContext().getPackageName());
        grxRightContainerView = (LinearLayout) findViewById(aux);
        grxOldKgSbOrder= "";//GrxSettings.sGrxKgSbViewsPositions;
        GrxSettings.readSbViesModsOptions(getContext());
        grxSetKgStatusBarViews(GrxSettings.sGrxKgSbViewsPositions);
    }

    public void grxSetKgStatusBarViews(String userselection){
        if(userselection==null || userselection.isEmpty()) return;
        if(grxOldKgSbOrder.equals(userselection)) return;
        grxOldKgSbOrder=userselection;
        GrxModsSharedMethods.grxRefreshSBViews(userselection,grxLeftContainerView,grxCenterContainerView,grxRightContainerView);
    }



}
