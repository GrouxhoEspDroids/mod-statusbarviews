package com.android.systemui.statusbar.phone;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;



import com.mods.grx.GrxModsSharedMethods;
import com.mods.grx.GrxSettings;

import grx.mods.com.mod_statusbarviews.R;

/**
 * Created by Grouxho 04/02/2018.
 */

public class StatusBarIconController {
    // do not add these vars to smali, they already exist!
    Context mContext;
    PhoneStatusBar mPhoneStatusBar;
    private ImageView mTwoPhoneModeIcon;
    private View mNotificationIconAreaInner;
    private LinearLayout mSystemIcon;
    private LinearLayout mSystemIconArea;


    // add these 3 vars to smali

    private LinearLayout grxCenterContainerView;
    private LinearLayout grxLeftContainerView;
    private String grxOldSbOrder;


    //sbicontroller constructor simulation

    public  StatusBarIconController(Context context, View baseview, PhoneStatusBar phoneStatusBar){

        // for convenience to be able to generate compatible smali code (code ready to use)
        // i am simulating the existing objects in samsung code that i need for the mod, using same vars names, etc.
        mContext = context;
        mPhoneStatusBar=phoneStatusBar;
        mSystemIcon = (LinearLayout) baseview.findViewById(R.id.system_icons);
        mNotificationIconAreaInner=baseview.findViewById(R.id.notification_icon_area_inner);
        mSystemIconArea=(LinearLayout) baseview.findViewById(R.id.system_icon_area);

        setTwoPhoneModeIcon(null); // do not add this, just  added if you do not know how to force compiling unused things

        //

        grxSetUPInitialPositions(baseview); // just add this call in smali constructor - We setup the vars and views
    }

    /** Looking inside the StatusBarIconController in stock s7e ui we have to move iconninnerarea inside system icons layout, to make views management mod easier */
    /* Initialization of containers and default value*/

    // add theses 2 methods to smali

    public void grxSetUPInitialPositions(View view){
       // grxOldSbOrder=GrxSettings.sGrxSbViewsPositions; /* default positions - GrxPerItemSingleSelection is the prefrerence to use */
        grxOldSbOrder="";
        int aux = mContext.getResources().getIdentifier("left_container","id",mContext.getPackageName());
        grxLeftContainerView = (LinearLayout) view.findViewById(aux);
        aux = mContext.getResources().getIdentifier("center_container","id",mContext.getPackageName());
        grxCenterContainerView=(LinearLayout) view.findViewById(aux);
        aux = mContext.getResources().getIdentifier("notification_icon_area","id",mContext.getPackageName());
        ViewGroup viewGroup = (ViewGroup) view.findViewById(aux);
        viewGroup.removeView(mNotificationIconAreaInner);
        grxLeftContainerView.addView(mNotificationIconAreaInner);
        GrxSettings.readSbViesModsOptions(mContext);
        grxSetStatusBarViews(GrxSettings.sGrxSbViewsPositions);
   }


   // add this method

    public void grxSetStatusBarViews(String userselection){
        if(userselection==null || userselection.isEmpty()) return;
        if(grxOldSbOrder.equals(userselection)) return;
        grxOldSbOrder=userselection;
        GrxModsSharedMethods.grxRefreshSBViews(userselection,grxLeftContainerView,grxCenterContainerView,mSystemIcon);
    }





// REPLACE THIS EXISTING METHOD IN SMALI

    public void setTwoPhoneModeIcon(ImageView imageView){ // we add a tag on fly to this view if it is created (it is created by code)

        // replace the method in smali by this one. We will add the icon to the end of systemIcons instead of systemicon area.
        // If the user has got a two sim device, this methiod will be called from phonestatusbar. Then we will add the icon and we will refresh the view.

        mTwoPhoneModeIcon=imageView;
        mTwoPhoneModeIcon.setTag("twophones");
        mSystemIcon.addView(mTwoPhoneModeIcon);
        GrxModsSharedMethods.grxRefreshSBViews(grxOldSbOrder,grxLeftContainerView,grxCenterContainerView,mSystemIcon);

    }


    // let´s hide and show the containers when they should


    /********* do not add the following 2 methods, it is just for convenience **/

    private void animateHide(View view, boolean bo){

    }
    private void animateShow(View view, boolean bo){

    }


    // REPLACE THese EXISTING METHODS

    /** these methods are called from PhoneStatusBar when the keyguard is showed or hiddien */
    // replace the 4 in smali
    public void hideSystemIconArea(boolean fake){
        animateHide(mSystemIconArea, fake);
        animateHide(grxCenterContainerView, false);
    }

    public void showSystemIconArea(boolean fake){
        animateHide(mSystemIconArea, fake);
        animateHide(grxCenterContainerView, false);
    }


        // replace this method, now our container is leftcontainer
    public void hideNotificationIconArea(boolean fake){
        animateHide(grxLeftContainerView, fake);
    }
    // replace this method, now our container is leftcontainer
    public void showNotificationIconArea(boolean fake){
        animateShow(grxLeftContainerView,fake);
    }



}
