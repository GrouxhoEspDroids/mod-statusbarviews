package com.android.systemui.statusbar.phone;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;

import com.mods.grx.GrxSettings;

/**
 * Created by Grouxho 04/02/2018.
 */
public class PhoneStatusBar {

    Context mContext;
    StatusBarIconController mIconController;
    KeyguardStatusBarView mKeyguardStatusBar;
    GrxObserver mGrxObserver;


    public PhoneStatusBar(Context context, View baseview){
        mContext=context;
        mIconController=new StatusBarIconController(mContext,baseview,this);

        mGrxObserver = new GrxObserver(new Handler());

    }

    public StatusBarIconController getIconController(){
        return mIconController;
    }


    public void grxOnStatusBarIconControllerCreated(){
        grxUpdateSbViewsMods();
    }

    public void grxUpdateSbViewsMods(){
        GrxSettings.readSbViesModsOptions(mContext);
        if(mIconController!=null) {
            mIconController.grxSetStatusBarViews(GrxSettings.sGrxSbViewsPositions);
        }
        if(mKeyguardStatusBar!=null){
            mKeyguardStatusBar.grxSetKgStatusBarViews(GrxSettings.sGrxKgSbViewsPositions);
        }
    }


    private class GrxObserver extends ContentObserver{

        private Uri mStatusBarViewsModsUri;


        public GrxObserver(android.os.Handler handler){
            super(handler);

            setUPKeys();
        }

        public void setUPKeys(){

            ContentResolver contentResolver = mContext.getContentResolver();
            mStatusBarViewsModsUri= Settings.System.getUriFor("gk_sb_viewspositions");
            contentResolver.registerContentObserver(mStatusBarViewsModsUri,false,this);
        }


        @Override
        public void onChange(boolean selfChange) {
            this.onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            if(uri.equals(mStatusBarViewsModsUri)) {
                grxUpdateSbViewsMods();
                return;
            }
        }

    }
}
